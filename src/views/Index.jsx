import React from "react";
import { 
  Collapse, 
  CardBody,
  FormGroup,
  Label,
  Alert,
} from 'reactstrap';

// Components
import Pessoa from '../components/Pessoa';
import ListaPessoa from '../components/ListaPessoa';
import FixedAlert from '../components/FixedAlert';

// Data
import data from '../data/data.json';


class Index extends React.Component {
  constructor(props) {
    super(props);
    let newData = localStorage.data !== undefined ? JSON.parse(localStorage.data) : data;
    
    this.state = {
      pessoas: newData,
      searchResults: newData,
      searchOpen: false,
      searchTerm: '',
      alertVisible: false,
      alertColor: 'danger',
      alertIcon: 'exclamation-triangle',
      alertTitle: 'Atenção!',
      alertText: 'Preencha todos os campos',
      alertTimeout: null,
    }

    this.searchInput = React.createRef();
    this.search = this.search.bind(this);
  }

  onShowAlert(title = 'Erro!', text = 'Algo deu errado', color = 'danger', icon = 'exclamation-triangle'){ 
    this.setState({
      alertTitle: title,
      alertText: text,
      alertColor: color,
      alertIcon: icon,
      alertVisible: true},()=>{
        clearTimeout(this.state.alertTimeout);
        this.setState({alertTimeout: window.setTimeout(()=>{
          this.setState({alertVisible:false})
        },4000)});
    });
  }

  attPessoa(pessoa){
    const newPessoa = pessoa.id === undefined;
    const pessoaId = newPessoa ? Object.keys(this.state.pessoas).length + 1 : pessoa.id;
    const msg = newPessoa ? 'Nova pessoa cadastrada.' : 'Cadastro atualizado.';
    
    const newState = {
      ...this.state,
      pessoas: {
        ...this.state.pessoas,
        [pessoaId]: {
          id: pessoaId,
          ...pessoa,
        },
      }
    };
    
    newState.searchResults = newState.pessoas;

    localStorage.setItem('data', JSON.stringify(newState.pessoas));
    this.setState(newState);
    this.onShowAlert('Sucesso!', msg, 'success', 'badge-check');
  }

  removePessoa(pessoa){
    let newState = {
      ...this.state,
    };

    delete newState.pessoas[pessoa.id];
    newState.searchResults = newState.pessoas;

    localStorage.setItem('data', JSON.stringify(newState.pessoas));
    this.setState(newState);
    this.onShowAlert('Usuário removido.','', 'danger', 'user-times');
  }

  toggleSearch(){
    if(!this.state.searchOpen){
      setTimeout(() => {
        this.searchInput.current.focus();
      }, 400);
    }
    this.setState({searchOpen: !this.state.searchOpen});
  }

  search(event){
    this.setState({searchTerm: event.target.value});
    
    const pessoas = this.state.pessoas;
    const search = event.target.value.toLowerCase();
    let matches = Object.values(pessoas).filter((a) => {
      return Object.values(a).filter(val => String(val).toLowerCase().includes(search)).length > 0;
    });

    this.setState({searchResults: matches});
  }

  render() {
    return (
      <>
        <main ref="main" className="row align-items-stretch mx-0">
          <div className="left-side col-12 col-lg-4 bg-gradient-primary min-height-100vh p-5">
            <div className="w-100 scroll-y">
              <h2 className="display-3 text-white mb-2"><i className="fad fa-fw fa-users-medical mr-2"></i> Cadastro</h2>
              <span className="text-white opacity-75 mb-4 d-block">Cadastre um novo usuário</span>
              <Pessoa new={true} onSubmit={this.attPessoa.bind(this)} />
            </div>
          </div>
          <div className="col p-5 bg-secondary">
            <div className="mt-n5 mx-n5 mb-5 bg-white">
              <Collapse isOpen={this.state.searchOpen}>
                <CardBody className="p-5">
                  <h2 className="display-3 mb-4"><i className="fad fa-fw fa-search mr-2"></i> Busca</h2>
                  <FormGroup>
                    <Label>Pesquise por qualquer campo</Label>
                    <input
                      className="form-control"
                      placeholder="Ex: São José do Rio Preto"
                      type="text"
                      name="search"
                      ref={this.searchInput}
                      value={this.state.searchTerm}
                      onChange={this.search}
                    />
                  </FormGroup>
                </CardBody>
              </Collapse>
            </div>
            <div className="w-100 scroll-y">
              <h2 className="display-3 mb-4"><i className="fad fa-fw fa-users mr-2"></i> Lista de usuários</h2>
              {Object.keys(this.state.searchResults).length > 0 ? (
                Object.keys(this.state.searchResults).map((item, index) => (
                  this.state.searchResults[item] ? (
                    <ListaPessoa pessoa={this.state.searchResults[item]} index={index} onSubmit={this.attPessoa.bind(this)} onRemove={this.removePessoa.bind(this)} key={item} />
                  ) : null
                ))
              ) : (
                <Alert color="danger">
                  <span className="alert-inner--icon">
                    <i className="fad fa-user-slash" />
                  </span>
                  <span className="alert-inner--text">
                    Nenhum usuário encontrado
                  </span>
                </Alert>
              )}
            </div>
          </div>
          <FixedAlert data={this.state} />
          <div className="search-button icon icon-shape icon-lg btn-primary rounded-circle" onClick={this.toggleSearch.bind(this)}>
            <i className="fa fa-fw fa-search" />
          </div>
        </main>
      </>
    );
  }
}

export default Index;
