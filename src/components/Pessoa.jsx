import React from "react";
import InputMask from "react-input-mask";
import classnames from "classnames";

// reactstrap components
import {
  FormGroup,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Button,
  Label,
  Modal,
} from "reactstrap";

class Pessoa extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pessoa: {
        ...this.props.pessoa
      },
      erros: {
        nome: '',
        nascimento: '',
        cpf: '',
        celular: '',
        email: '',
        endereco: '',
      },
      isNew: this.props.new,
      removeModal: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {    
    let pessoa = this.state.pessoa;
    let erros = this.state.erros;
    pessoa[event.target.name] = event.target.value;        
    erros[event.target.name] = '';
    this.setState({pessoa});
  }

  handleValidation() {
    let pessoa = this.state.pessoa;
    let erros = {};
    let formIsValid = true;
    let nome = pessoa.nome;
    let nascimento = pessoa.nascimento;
    let cpf = pessoa.cpf;
    let celular = pessoa.celular;
    let email = pessoa.email;
    let endereco = pessoa.endereco;

    if(typeof nome !== "undefined"){
      if(!nome.match(/^[A-Za-z\s\u00C0-\u00FF]+$/)){
        formIsValid = false;
        erros["nome"] = "Apenas letras";
      }        
    }

    if(!nome){
      formIsValid = false;
      erros["nome"] = "Campo obrigatório";
    }

    if(!nascimento || nascimento === '__/__/____'){
      formIsValid = false;
      erros["nascimento"] = "Campo obrigatório";
    } else if(typeof nascimento !== "undefined"){
      function ultimoDiaDoMes(mes){
        if(mes === 1 || mes === 3 || mes === 5 || mes === 7 || mes === 8 || mes === 10 || mes === 12){
          return 31;
        } else if(mes === 2){
          return 29;
        } else {
          return 30;
        }
      }
      
      const dia = Number(nascimento.split('/')[0]);
      const mes = Number(nascimento.split('/')[1]);
      const ano = Number(nascimento.split('/')[2]);
      const validDate = dia <= 31 && mes <= 12 && ano >= 1900 && ano <= 2020 && dia <= ultimoDiaDoMes(mes);
      
      if(!validDate || !nascimento.match(/^[0-9][0-9]\/[0-9][0-9]\/[0-9][0-9][0-9][0-9]$/)){
        formIsValid = false;
        erros["nascimento"] = "Data inválida";
      }
    }

    if(!cpf || cpf === '___.___.___-__'){
      formIsValid = false;
      erros["cpf"] = "Campo obrigatório";
    } else if(typeof cpf !== "undefined"){
      function validCPF(strCPF) {
        var Soma;
        var Resto;
        var i;
        Soma = 0;
        if (strCPF === "00000000000") return false;
          
        for (i=1; i<=9; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
        Resto = (Soma * 10) % 11;
        
          if ((Resto === 10) || (Resto === 11))  Resto = 0;
          if (Resto !== parseInt(strCPF.substring(9, 10)) ) return false;
        
        Soma = 0;
          for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
          Resto = (Soma * 10) % 11;
        
          if ((Resto === 10) || (Resto === 11))  Resto = 0;
          if (Resto !== parseInt(strCPF.substring(10, 11) ) ) return false;
          return true;
      }
      if(!validCPF(cpf.replace(/[^0-9]+/g, ""))){
        formIsValid = false;
        erros["cpf"] = "CPF inválido";
      }        
    }

    if(!celular || celular === '(__) _____-____'){
      formIsValid = false;
      erros["celular"] = "Campo obrigatório";
    } else if(typeof celular !== "undefined"){
      if(!celular.replace(/[^0-9]+/g, "").match(/^[0-9][0-9][9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$/)){
        formIsValid = false;
        erros["celular"] = "Número inválido";
      }
    }

    if(typeof email !== "undefined"){
      let lastAtPos = email.lastIndexOf('@');
      let lastDotPos = email.lastIndexOf('.');

      if (!(lastAtPos < lastDotPos && lastAtPos > 0 && email.indexOf('@@') === -1 && lastDotPos > 2 && (email.length - lastDotPos) > 2)) {
        formIsValid = false;
        erros["email"] = "E-mail inválido";
      }
    } 

    if(!email){
       formIsValid = false;
       erros["email"] = "Campo obrigatório";
    } 

    if(!endereco){
       formIsValid = false;
       erros["endereco"] = "Campo obrigatório";
    } 

   this.setState({erros: erros});
   return formIsValid;
  }

  handleSubmit(event) {
    event.preventDefault();
    
    if(this.handleValidation()){
      this.props.onSubmit(this.state.pessoa);
      if(this.state.isNew){
        this.setState({
          pessoa: {
            nome: '',
            nascimento: '',
            cpf: '',
            celular: '',
            email: '',
            endereco: '',
            observacoes: '',
          },
          erros: {
            nome: '',
            nascimento: '',
            cpf: '',
            celular: '',
            email: '',
            endereco: '',
          }
        });
      }
    }else{
      //console.log('erros');
    }
    return false;
  }

  handleRemove(event) {
    event.preventDefault();
    this.props.onRemove(this.state.pessoa);
    this.toggleModal("removeModal");
  }
  
  toggleModal = state => {
    this.setState({
      [state]: !this.state[state]
    });
  };

  render() {
    return (
      <>
        <form className="row" onSubmit={this.handleSubmit}>
          <div className={this.state.isNew ? 'col-12' : 'col-6'}>
            <FormGroup>
              <Label className="text-white opacity-75">Nome</Label>
              <InputGroup className={classnames([(this.state.isNew ? "input-group-alternative " : "") + (this.state.erros["nome"] ? "is-invalid" : "")])}>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>
                    <i className={classnames(["fad fa-fw fa-user text-" + (this.state.erros["nome"] ? "danger" : "primary")])} />
                  </InputGroupText>
                </InputGroupAddon>
                <Input
                  className={classnames([this.state.isNew ? "form-control-alternative" : ""])}
                  placeholder="Ex: João Silva"
                  type="text"
                  name="nome"
                  value={this.state.pessoa.nome}
                  onChange={this.handleChange}
                />
              </InputGroup>
              <small className="invalid-feedback badge badge-danger">{this.state.erros["nome"]}</small>
            </FormGroup>
          </div>
          <div className={this.state.isNew ? 'col-12' : 'col-6'}>
            <FormGroup>
              <Label className="text-white opacity-75">Data de nascimento</Label>
              <InputGroup className={classnames([(this.state.isNew ? "input-group-alternative " : "") + (this.state.erros["nascimento"] ? "is-invalid" : "")])}>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>
                    <i className={classnames(["fad fa-fw fa-calendar-star text-" + (this.state.erros["nascimento"] ? 'danger' : 'primary')])} />
                  </InputGroupText>
                </InputGroupAddon>
                <InputMask
                  className={classnames(["form-control " + (this.state.isNew ? "form-control-alternative" : "")])}
                  placeholder="__/__/____"
                  mask="99/99/9999"
                  type="text"
                  name="nascimento"
                  value={this.state.pessoa.nascimento}
                  onChange={this.handleChange}
                />
              </InputGroup>
              <small className="invalid-feedback badge badge-danger">{this.state.erros["nascimento"]}</small>
            </FormGroup>
          </div>
          <div className={this.state.isNew ? 'col-12' : 'col-6'}>
            <FormGroup>
              <Label className="text-white opacity-75">CPF</Label>
              <InputGroup className={classnames([(this.state.isNew ? "input-group-alternative " : "") + (this.state.erros["cpf"] ? "is-invalid" : "")])}>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>
                    <i className={classnames(["fad fa-fw fa-id-card text-" + (this.state.erros["cpf"] ? 'danger' : 'primary')])} />
                  </InputGroupText>
                </InputGroupAddon>
                <InputMask
                  className={classnames(["form-control " + (this.state.isNew ? "form-control-alternative" : "")])}
                  placeholder="___.___.___-__"
                  mask="999.999.999-**"
                  type="text"
                  name="cpf"
                  value={this.state.pessoa.cpf}
                  onChange={this.handleChange}
                />
              </InputGroup>
              <small className="invalid-feedback badge badge-danger">{this.state.erros["cpf"]}</small>
            </FormGroup>
          </div>
          <div className={this.state.isNew ? 'col-12' : 'col-6'}>
            <FormGroup>
              <Label className="text-white opacity-75">Celular</Label>
              <InputGroup className={classnames([(this.state.isNew ? "input-group-alternative " : "") + (this.state.erros["celular"] ? "is-invalid" : "")])}>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>
                    <i className={classnames(["fad fa-fw fa-phone text-" + (this.state.erros["celular"] ? 'danger' : 'primary')])} />
                  </InputGroupText>
                </InputGroupAddon>
                <InputMask
                  className={classnames(["form-control " + (this.state.isNew ? "form-control-alternative" : "")])}
                  placeholder="(__) _____-____"
                  mask="(99) 99999-9999"
                  type="tel"
                  name="celular"
                  value={this.state.pessoa.celular}
                  onChange={this.handleChange}
                />
              </InputGroup>
              <small className="invalid-feedback badge badge-danger">{this.state.erros["celular"]}</small>
            </FormGroup>
          </div>
          <div className={this.state.isNew ? 'col-12' : 'col-6'}>
            <FormGroup>
              <Label className="text-white opacity-75">E-mail</Label>
              <InputGroup className={classnames([(this.state.isNew ? "input-group-alternative " : "") + (this.state.erros["email"] ? "is-invalid" : "")])}>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>
                    <i className={classnames(["fad fa-fw fa-envelope text-" + (this.state.erros["email"] ? 'danger' : 'primary')])} />
                  </InputGroupText>
                </InputGroupAddon>
                <Input
                  className={classnames([this.state.isNew ? "form-control-alternative" : ""])}
                  placeholder="Ex: joaosilva@gmail.com"
                  type="email"
                  name="email"
                  value={this.state.pessoa.email}
                  onChange={this.handleChange}
                />
              </InputGroup>
              <small className="invalid-feedback badge badge-danger">{this.state.erros["email"]}</small>
            </FormGroup>
          </div>
          <div className={this.state.isNew ? 'col-12' : 'col-6'}>
            <FormGroup>
              <Label className="text-white opacity-75">Endereço</Label>
              <InputGroup className={classnames([(this.state.isNew ? "input-group-alternative " : "") + (this.state.erros["endereco"] ? "is-invalid" : "")])}>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>
                    <i className={classnames(["fad fa-fw fa-map-marker-alt text-" + (this.state.erros["endereco"] ? 'danger' : 'primary')])} />
                  </InputGroupText>
                </InputGroupAddon>
                <Input
                  className={classnames([this.state.isNew ? "form-control-alternative" : ""])}
                  placeholder="Ex: Rua Minas Gerais, 374, Vila Bom Jesus"
                  type="text"
                  name="endereco"
                  value={this.state.pessoa.endereco}
                  onChange={this.handleChange}
                />
              </InputGroup>
              <small className="invalid-feedback badge badge-danger">{this.state.erros["endereco"]}</small>
            </FormGroup>
          </div>
          <div className="col-12">
            <FormGroup>
              <Label className="text-white opacity-75">Observações (Opcional)</Label>
              <InputGroup className={classnames([this.state.isNew ? "input-group-alternative " : ""])}>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>
                    <i className="text-primary fad fa-fw fa-highlighter" />
                  </InputGroupText>
                </InputGroupAddon>
                <Input
                  className={classnames([this.state.isNew ? "form-control-alternative" : ""])}
                  placeholder="Ex: Estudou na faculdade UNIP"
                  type="textarea"
                  maxLength="300"
                  name="observacoes"
                  value={this.state.pessoa.observacoes}
                  onChange={this.handleChange}
                />
              </InputGroup>
            </FormGroup>
          </div>
          <div className="col-12 mt-4">
            {this.state.isNew ? (
              <Button block color="success" size="lg" type="submit">
                Cadastrar
              </Button>
            ) : (
              <div className="row">
                <div className="col-6">
                  <Button block color="danger" outline size="lg" onClick={() => this.toggleModal("removeModal")}>
                    Remover
                  </Button>
                  <Modal
                    className="modal-dialog-centered modal-danger"
                    contentClassName="bg-gradient-danger"
                    isOpen={this.state.removeModal}
                    toggle={() => this.toggleModal("removeModal")}
                  >
                    <div className="modal-header">
                      <button
                        aria-label="Close"
                        className="close"
                        data-dismiss="modal"
                        type="button"
                        onClick={() => this.toggleModal("removeModal")}
                      >
                        <i className="fa fa-times small text-white"></i>
                      </button>
                    </div>
                    <div className="modal-body">
                      <div className="py-3 text-center">
                        <i className="fad fa-exclamation-triangle ni-3x mb-4" />
                        <h4 className="text-white mb-2">Tem certeza que deseja remover o usuário?</h4>
                        <p>Essa ação não poderá ser desfeita.</p>
                      </div>
                    </div>
                    <div className="modal-footer">
                      <Button
                        className="text-white mr-auto"
                        color="link"
                        data-dismiss="modal"
                        type="button"
                        onClick={() => this.toggleModal("removeModal")}
                      >
                        Fechar
                      </Button>
                      <Button className="btn-white" color="default" type="button" onClick={this.handleRemove.bind(this)}>
                        Remover
                      </Button>
                    </div>
                  </Modal>
                </div>
                <div className="col-6">
                  <Button block color="success" outline size="lg" type="submit">
                    Alterar
                  </Button>
                </div>
              </div>
            )}
          </div>
        </form>
      </>
    );
  }
}

export default Pessoa;
