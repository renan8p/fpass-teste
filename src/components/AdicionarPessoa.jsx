import React from "react";
import InputMask from "react-input-mask";
import classnames from "classnames";

// reactstrap components
import {
  FormGroup,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Button,
  Label,
} from "reactstrap";

class AdicionarPessoa extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: {
        nome: '',
        nascimento: '',
        cpf: '',
        celular: '',
        email: '',
        endereco: '',
        observacoes: '',
      },
      erros: {
        nome: '',
        nascimento: '',
        cpf: '',
        celular: '',
        email: '',
        endereco: '',
      }
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  invalidateInput(input) {
    return 'is-invalid';
  }

  handleChange(event) {    
    let fields = this.state.fields;
    let erros = this.state.erros;
    fields[event.target.name] = event.target.value;        
    erros[event.target.name] = '';
    this.setState({fields});
  }

  handleValidation() {
    let fields = this.state.fields;
    let erros = {};
    let formIsValid = true;
    let nome = fields.nome;
    let nascimento = fields.nascimento;
    let cpf = fields.cpf;
    let celular = fields.celular;
    let email = fields.email;
    let endereco = fields.endereco;

    if(typeof nome !== "undefined"){
      if(!nome.match(/^[A-Za-z\s\u00C0-\u00FF]+$/)){
        formIsValid = false;
        erros["nome"] = "Apenas letras";
      }        
    }

    if(!nome){
      formIsValid = false;
      erros["nome"] = "Campo obrigatório";
    }

    if(!nascimento || nascimento === '__/__/____'){
      formIsValid = false;
      erros["nascimento"] = "Campo obrigatório";
    } else if(typeof nascimento !== "undefined"){
      function ultimoDiaDoMes(mes){
        if(mes === 1 || mes === 3 || mes === 5 || mes === 7 || mes === 8 || mes === 10 || mes === 12){
          return 31;
        } else if(mes === 2){
          return 29;
        } else {
          return 30;
        }
      }
      
      const dia = Number(nascimento.split('/')[0]);
      const mes = Number(nascimento.split('/')[1]);
      const ano = Number(nascimento.split('/')[2]);
      const validDate = dia <= 31 && mes <= 12 && ano >= 1900 && ano <= 2020 && dia <= ultimoDiaDoMes(mes);
      
      if(!validDate || !nascimento.match(/^[0-9][0-9]\/[0-9][0-9]\/[0-9][0-9][0-9][0-9]$/)){
        formIsValid = false;
        erros["nascimento"] = "Data inválida";
      }
    }

    if(!cpf || cpf === '___.___.___-__'){
      formIsValid = false;
      erros["cpf"] = "Campo obrigatório";
    } else if(typeof cpf !== "undefined"){
      function validCPF(strCPF) {
        var Soma;
        var Resto;
        var i;
        Soma = 0;
        if (strCPF === "00000000000") return false;
          
        for (i=1; i<=9; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
        Resto = (Soma * 10) % 11;
        
          if ((Resto === 10) || (Resto === 11))  Resto = 0;
          if (Resto !== parseInt(strCPF.substring(9, 10)) ) return false;
        
        Soma = 0;
          for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
          Resto = (Soma * 10) % 11;
        
          if ((Resto === 10) || (Resto === 11))  Resto = 0;
          if (Resto !== parseInt(strCPF.substring(10, 11) ) ) return false;
          return true;
      }
      if(!validCPF(cpf.replace(/[^0-9]+/g, ""))){
        formIsValid = false;
        erros["cpf"] = "CPF inválido";
      }        
    }

    if(!celular || celular === '(__) _____-____'){
      formIsValid = false;
      erros["celular"] = "Campo obrigatório";
    } else if(typeof celular !== "undefined"){
      if(!celular.replace(/[^0-9]+/g, "").match(/^[0-9][0-9][9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$/)){
        formIsValid = false;
        erros["celular"] = "Número inválido";
      }
    }

    if(typeof email !== "undefined"){
      let lastAtPos = email.lastIndexOf('@');
      let lastDotPos = email.lastIndexOf('.');

      if (!(lastAtPos < lastDotPos && lastAtPos > 0 && email.indexOf('@@') === -1 && lastDotPos > 2 && (email.length - lastDotPos) > 2)) {
        formIsValid = false;
        erros["email"] = "E-mail inválido";
      }
    } 

    if(!email){
       formIsValid = false;
       erros["email"] = "Campo obrigatório";
    } 

    if(!endereco){
       formIsValid = false;
       erros["endereco"] = "Campo obrigatório";
    } 

   this.setState({erros: erros});
   return formIsValid;
  }

  handleSubmit(event) {
    event.preventDefault();
    
    if(this.handleValidation()){
      this.props.novaPessoa(this.state.fields);
      this.setState({
        fields: {
          nome: '',
          nascimento: '',
          cpf: '',
          celular: '',
          email: '',
          endereco: '',
          observacoes: '',
        },
        erros: {
          nome: '',
          nascimento: '',
          cpf: '',
          celular: '',
          email: '',
          endereco: '',
        }
      });
    }else{
      //console.log('erros');
    }
    return false;
  }

  render() {
    return (
      <>
        <form onSubmit={this.handleSubmit}>
          <FormGroup>
            <Label className="text-white opacity-75">Nome</Label>
            <InputGroup className={classnames(["input-group-alternative " + (this.state.erros["nome"] ? 'is-invalid' : '')])}>
              <InputGroupAddon addonType="prepend">
                <InputGroupText>
                  <i className={classnames(["fad fa-fw fa-user text-" + (this.state.erros["nome"] ? 'danger' : 'primary')])} />
                </InputGroupText>
              </InputGroupAddon>
              <Input
                className="form-control form-control-alternative"
                placeholder="Ex: João Silva"
                type="text"
                name="nome"
                value={this.state.fields.nome}
                onChange={this.handleChange}
              />
            </InputGroup>
            <small className="invalid-feedback badge badge-danger">{this.state.erros["nome"]}</small>
          </FormGroup>
          <FormGroup>
            <Label className="text-white opacity-75">Data de nascimento</Label>
            <InputGroup className={classnames(["input-group-alternative " + (this.state.erros["nascimento"] ? 'is-invalid' : '')])}>
              <InputGroupAddon addonType="prepend">
                <InputGroupText>
                  <i className={classnames(["fad fa-fw fa-calendar-star text-" + (this.state.erros["nascimento"] ? 'danger' : 'primary')])} />
                </InputGroupText>
              </InputGroupAddon>
              <InputMask
                className="form-control form-control-alternative"
                placeholder="__/__/____"
                mask="99/99/9999"
                type="text"
                name="nascimento"
                value={this.state.fields.nascimento}
                onChange={this.handleChange}
              />
            </InputGroup>
            <small className="invalid-feedback badge badge-danger">{this.state.erros["nascimento"]}</small>
          </FormGroup>
          <FormGroup>
            <Label className="text-white opacity-75">CPF</Label>
            <InputGroup className={classnames(["input-group-alternative " + (this.state.erros["cpf"] ? 'is-invalid' : '')])}>
              <InputGroupAddon addonType="prepend">
                <InputGroupText>
                  <i className={classnames(["fad fa-fw fa-id-card text-" + (this.state.erros["cpf"] ? 'danger' : 'primary')])} />
                </InputGroupText>
              </InputGroupAddon>
              <InputMask
                className="form-control form-control-alternative"
                placeholder="___.___.___-__"
                mask="999.999.999-**"
                type="text"
                name="cpf"
                value={this.state.fields.cpf}
                onChange={this.handleChange}
              />
            </InputGroup>
            <small className="invalid-feedback badge badge-danger">{this.state.erros["cpf"]}</small>
          </FormGroup>
          <FormGroup>
            <Label className="text-white opacity-75">Celular</Label>
            <InputGroup className={classnames(["input-group-alternative " + (this.state.erros["celular"] ? 'is-invalid' : '')])}>
              <InputGroupAddon addonType="prepend">
                <InputGroupText>
                  <i className={classnames(["fad fa-fw fa-phone text-" + (this.state.erros["celular"] ? 'danger' : 'primary')])} />
                </InputGroupText>
              </InputGroupAddon>
              <InputMask
                className="form-control form-control-alternative"
                placeholder="(__) _____-____"
                mask="(99) 99999-9999"
                type="tel"
                name="celular"
                value={this.state.fields.celular}
                onChange={this.handleChange}
              />
            </InputGroup>
            <small className="invalid-feedback badge badge-danger">{this.state.erros["celular"]}</small>
          </FormGroup>
          <FormGroup>
            <Label className="text-white opacity-75">E-mail</Label>
            <InputGroup className={classnames(["input-group-alternative " + (this.state.erros["email"] ? 'is-invalid' : '')])}>
              <InputGroupAddon addonType="prepend">
                <InputGroupText>
                  <i className={classnames(["fad fa-fw fa-envelope text-" + (this.state.erros["email"] ? 'danger' : 'primary')])} />
                </InputGroupText>
              </InputGroupAddon>
              <Input
                className="form-control form-control-alternative"
                placeholder="Ex: joaosilva@gmail.com"
                type="email"
                name="email"
                value={this.state.fields.email}
                onChange={this.handleChange}
              />
            </InputGroup>
            <small className="invalid-feedback badge badge-danger">{this.state.erros["email"]}</small>
          </FormGroup>
          <FormGroup>
            <Label className="text-white opacity-75">Endereço</Label>
            <InputGroup className={classnames(["input-group-alternative " + (this.state.erros["endereco"] ? 'is-invalid' : '')])}>
              <InputGroupAddon addonType="prepend">
                <InputGroupText>
                  <i className={classnames(["fad fa-fw fa-map-marker-alt text-" + (this.state.erros["endereco"] ? 'danger' : 'primary')])} />
                </InputGroupText>
              </InputGroupAddon>
              <Input
                className="form-control form-control-alternative"
                placeholder="Ex: Rua Minas Gerais, 374, Vila Bom Jesus"
                type="text"
                name="endereco"
                value={this.state.fields.endereco}
                onChange={this.handleChange}
              />
            </InputGroup>
            <small className="invalid-feedback badge badge-danger">{this.state.erros["endereco"]}</small>
          </FormGroup>
          <FormGroup>
            <Label className="text-white opacity-75">Observações (Opcional)</Label>
            <InputGroup className="input-group-alternative">
              <InputGroupAddon addonType="prepend">
                <InputGroupText>
                  <i className="text-primary fad fa-fw fa-highlighter" />
                </InputGroupText>
              </InputGroupAddon>
              <Input
                className="form-control form-control-alternative"
                placeholder="Ex: Estudou na faculdade UNIP"
                type="textarea"
                maxLength="300"
                name="observacoes"
                value={this.state.fields.observacoes}
                onChange={this.handleChange}
              />
            </InputGroup>
          </FormGroup>
          <Button block color="success" size="lg" type="submit" className="mt-4">
            Cadastrar
          </Button>
        </form>
      </>
    );
  }
}

export default AdicionarPessoa;
