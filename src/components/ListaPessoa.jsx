import React from 'react';
import classnames from "classnames";
// reactstrap components
import { 
  Collapse, 
  CardBody,
} from 'reactstrap';

// Components
import Pessoa from '../components/Pessoa';

class ListaPessoas extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      colors: ['primary', 'info', 'warning', 'success', 'danger','default'],
      isOpen: false,
    };
    
    //console.log(this.state.data)
    this.toggle = this.toggle.bind(this);
  }

  toggle(){
    this.setState({isOpen: !this.state.isOpen});
  }

  onSubmit(newPessoa){
    this.props.onSubmit(newPessoa);
    this.toggle();
  }

  render() {
    return (
      <>
        <div className="card shadow-sm border-0 mb-4">
          <div className="card-body cursor-pointer" onClick={this.toggle}>
            <div className="row align-items-center">
              <div className="col-auto">
                <div className={classnames(['icon icon-shape rounded-circle icon-shape-' + this.state.colors[this.props.index % this.state.colors.length]])}><i className="fad fa-user"></i></div>
              </div>
              <div className="col pl-0">
                <h5 className={classnames(['m-0 font-weight-bold text-' + this.state.colors[this.props.index % this.state.colors.length]])}>{this.props.pessoa.nome}</h5>
              </div>
              <div className="col-auto">
                <i className={classnames(['fa fa-fw fa-chevron-' + (this.state.isOpen ? 'up' : 'down')])}></i>
              </div>
            </div>
          </div>
          <Collapse isOpen={this.state.isOpen}>
            <CardBody className="border-top">
              <Pessoa pessoa={this.props.pessoa} onSubmit={this.onSubmit.bind(this)} onRemove={this.props.onRemove.bind(this)} />
            </CardBody>
          </Collapse>
        </div>
      </>
    );
  }
}

export default ListaPessoas;
