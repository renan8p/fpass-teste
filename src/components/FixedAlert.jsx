/*!

=========================================================
* Argon Design System React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-design-system-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-design-system-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";

// Components
import { Alert } from 'reactstrap';
import classnames from "classnames";


class FixedAlert extends React.Component {
  render() {
    return (
      <>
        <Alert className="fixed-alert" color={this.props.data.alertColor} isOpen={this.props.data.alertVisible} fade={true}>
          <span className="alert-inner--icon">
            <i className={classnames("fad", ['fa-' + this.props.data.alertIcon])} />
          </span>
          <span className="alert-inner--text">
            <strong>{this.props.data.alertTitle}</strong> {this.props.data.alertText}
          </span>
        </Alert>
      </>
    );
  }
}

export default FixedAlert;
